# Fallout Equestria: REMAINS

This is a mirror site for [the main site](http://foe.ucoz.org)

<div align="center">
![Main menu image](/public/pic/mainmenu.png "Fallout Equestria: REMAINS")
</div>

[Fallout Equestria: REMAINS](https://foe-remains.gitlab.io) is a hardcore mix of platformer, 2D-shooter and roguelike with RPG elements brought to you by devoted fans of [Fallout: Equestria](https://www.fimfiction.net/story/119190/fallout-equestria) universe authored by [Kkat](https://www.fimfiction.net/user/10369/Kkat).

Travel around a post-apocalyptic world and explore its various corners, communicate with locals and help them, level up and fight enemies using a vast arsenal of weapons and magic. The full playthrough offers 30+ hours of fascinating experience.

This is a completely free game, no pay-to-win.

## You can play it right now!

### [Play in browser](https://foe-remains.gitlab.io/remains.html)

You need an installed Flash-player to play the game.

### [Download Windows installer](https://drive.google.com/open?id=1m8k_yV1zFC-KmQafSGHaCipeuAsEjqPr)

This is a preferred option for Windows users, this way the game runs smoother.

### [Download portable archive](https://drive.google.com/open?id=1ECmKWopruIEXoIpmuLuZ5R4Woj2qL3aa)

Extract the archive to the destination of your choice, you can run the game using pfe.swf (via [Stand-alone Flash-player](https://fpdownload.macromedia.com/pub/flashplayer/updaters/32/flashplayer_32_sa.exe)) or pfe.exe.

If exe- or swf-version of the game are outdated, you can update them by placing [this file](https://foe-remains.gitlab.io/pfe.swf) in your game directory.

## Gameplay trailer

[![https://www.youtube.com/watch?v=ru57HxJnl74/0.jpg](http://img.youtube.com/vi/ru57HxJnl74/0.jpg)](http://www.youtube.com/watch?v=ru57HxJnl74 "Gameplay trailer")

## About the game

Fallout Equestria: REMAINS is created by a team of enthusiasts led by [Empalu](https://www.deviantart.com/empalu). The idea of this game originates from Fallout: Equestria book authored by Kkat.

Fallout: Equestria is a crossover between Fallout game series and My Little Pony: Friendship is Magic show. In a nutshell, it is "post-apocalypse with ponies", but the book itself and a lot of derived fan works forms a whole alive universe.

REMAINS has no direct relationship neither with "Fallout" series nor with the show about ponies, though, of course, some references occur here and there.

The game takes its place in the Equestrian Wasteland, but its storyline is not connected with the book's one, it is a standalone work based on the same universe. If you want to learn more about history of this world, the Great War and a following apocalypse, you should familiarize yourself with Fallout: Equestria.

## Visit our Wiki

[Fallout Equestria: Remains Wiki](http://ru.fallout-equestria-remains.wikia.com/wiki/Fallout_Equestria:_Remains_%D0%B2%D0%B8%D0%BA%D0%B8) (in Russian)

[Fallout Equestria: Remains Wiki](https://fallout-equestria-remains.fandom.com/wiki/Fallout_Equestria:_Remains_Wiki) (in English)

Learn more about the game (or help us to fill our Wiki with relevant information if you already know everything)

## Join our Discord channel

[Here](https://discord.gg/qWYAbsp) you can always ask your question or chat with other players and the developers
<div align="center">
![Logo](/public/pic/foelogo2.png "FoE: REMAINS logo")
</div>
